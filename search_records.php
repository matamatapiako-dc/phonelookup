<?php
require_once("../authorityphp/authority.php");

$authority = new authority();
$authority->login();
$nar_api = $authority->initialise('NA');

if ($_REQUEST['orgName'] != "") {
    $orgName = $authority->replace_special_characters($_REQUEST['orgName']);
    $filter = "startswith(familyName, '{$orgName}')";
} else if ($_REQUEST['familyName'] != "" && $_REQUEST['givenName'] != "") {
    $familyName = $authority->replace_special_characters($_REQUEST['familyName']);
    $filter = "startswith(givenName1, '{$_REQUEST['givenName']}') AND startswith(familyName, '{$familyName}') AND title ne 'STAFF'";
    $order = "givenName1";
} else if ($_REQUEST['familyName'] != "") {
    $familyName = $authority->replace_special_characters($_REQUEST['familyName']);
    $filter = "startswith(familyName, '{$familyName}') AND title ne 'STAFF'";
    $order = "givenName1";
} else if ($_REQUEST['givenName'] != "") {
    $filter = "startswith(givenName1, '{$_REQUEST['givenName']}') AND title ne 'STAFF'";
    $order = "familyName";
}

$result = $nar_api->getNarRecords($filter, $order);

if ($result) {
    echo json_encode($result);
} else {
    echo "false";
}
