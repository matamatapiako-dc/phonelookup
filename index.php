<?php

include_once("config.php");
include_once("../authorityphp/authority.php");

$authority = new authority();
$authority->login();

$links = new authority_namelinks($authority);
$docTypes = new authority_documenttypes($authority);
$request_types = array();
$dtypes = $docTypes->documenttypes();

$number_of_years_history = 3;
$show_years = array();

if (isset($_GET['showYears'])) {
    $number_of_years_history = $_GET['showYears'];
}

for ($i = 0; $i != $number_of_years_history; $i++) {
    $show_years[] = date("Y") - $i;
}

$exclude_types = array("191", "192");

foreach ($dtypes as $dt) {
    $request_types[$dt['type']] = $dt['description'];
}

$html = '
<!DOCTYPE html>
 <html>
  <head>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
  </head>
  <body>
 ';

$tabs = '<div class="tab-content" id="myTabContent">';
$nav = '<nav class="navbar navbar-light bg-light">
            <div class="container">
            <ul class="nav nav-pills">
            <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown"
                            href="#">Records</a>
                    <div class="dropdown-menu">';

if (isset($_GET['phone']) || isset($_GET['email']) || isset($_GET['firstname']) || isset($_GET['lastname'])) 
{

    if (isset($_GET['phone'])) {
        require_once("helpers/phone.php");

        $phone = new PhoneHelper($_GET['phone']);
        $results = $phone->getNarRecords();
    } else if (isset($_GET['email'])) {
        require_once("helpers/email.php");

        $email = new EmailHelper($_GET['email']);
        $results = $email->getNarRecords();
    } else if (isset($_GET['firstname']) || isset($_GET['lastname'])) {
        require_once("helpers/name.php");

        $name = new NameHelper($_GET['firstname'], $_GET['lastname']);
        $results = $name->getNarRecords();
    } else {
        die();
    }

    $fh = fopen("logs/number_search_log_" . date("Ymd") . ".txt", "a");
    $log_string = date("H:i:s") . "   " . $value . "   " . $_GET['phone'] . "   " . count($results) . " results found \r\n";
    fwrite($fh, $log_string);
    fclose($fh);

    $updateopts = '';

    if ($results != false) {
        $isactive = 'active';
        foreach ($results as $index=>$name) {

            $history_records = 0;
            $tabs .= '<div class="tab-pane ' . $isactive . '" id="page' . $name['id'] . '" role="tabpanel" aria-labelledby="tab' . $name['id'] . '"><div class="card">
            <div class="card-body">
            <h3 class="card-title">' . ($name['givenName1'] != '' ? $name['givenName1'] . ' ' . $name['givenName2'] . ' ' . $name['familyName'] : $name['organisationName']) . '</h3>
            <p class="card-text">
                <strong> NAR Number: </strong> ' . $name['id'] . '<br />
                ' . ($name['homePhone1'] != '' ? '<strong> Home Phone: </strong> ' . $name['homePhone1'] . ' ' . ($name['homePhone2'] != '' ? ' (alternatively ' . $name['homePhone2'] . ')<br />' : '<br />') : '') . '
                ' . ($name['mobilePhone'] != '' ? '<strong> Mobile Phone: </strong> <a target="_parent" href="tel:' . $name['mobilePhone'] . '">' . $name['mobilePhone'] . ' </a><br />' : '') . '
                ' . ($name['businessPhone'] != '' ? '<strong> Business Phone: </strong> ' . $name['businessPhone'] . ' <br />' : '') . '
                ' . ($name['emailAddress'] != '' ? '<strong> Email: </strong> ' . $name['emailAddress'] . '<br />' : '') . '
            </p>';
            //var_dump(array_keys($name));
            $nlList = '';
            $name_links = $links->getNarNameLinks("NarId eq " . $name['id']);
            $name_links = array_reverse($name_links);
            if ($name_links != false) {
                foreach ($name_links as $l) {
                    if ($l['moduleReference'] == 'DD') {
                        list($type, $year, $docnum, $part) = explode(".", $l['formattedAccount']);
                        if (in_array($year, $show_years) && !in_array($type, $exclude_types)) {
                            $nlList .= '<p data-fma="' . $l['formattedAccount'] . '" id="' . str_replace('.', '', $l['formattedAccount']) . '" class="listItem">' . $request_types[ltrim($type, "0")] . ' - ' . $l['formattedAccount'] . ' <a href="#" data-fma="' . str_replace('.', '', $l['formattedAccount']) . '" class="view_details btn btn-primary"><span>View Details</span></a></p>';
                            $history_records++;
                        } else if (strlen($year) != 0) {
                            //$html .= "$year is not " . date("Y") . '<br />';
                        } else {
                            $nlList .= var_export($l, true);
                        }
                    }
                }
            }

            $tabs .= '<p id="recordCount"><strong>Found ' . $history_records . ' records. Loading record data...</strong></p>' . $nlList;

            $tabs .= ' </div></div></div>';

            $nav .= '<a class="dropdown-item ' . $isactive . '" id="tab' . $name['id'] . '">' . ($name['givenName1'] != '' ? $name['givenName1'] . ' ' . $name['familyName'] : $name['organisationName']) . ' (' . $history_records  . ')</a>';

            //$optidx = $index + 1;
            $updateopts .= '<option class="result-item" value="' . $index . '">' . ($name['givenName1'] != '' ? $name['givenName1'] . ' ' . $name['familyName'] : $name['organisationName']) . '</option>';
            $isactive = '';
        }

        $nav .= '</div></li>';

    }
    else
    {
        $isactive = 'active';
        $nav .= '<a class="dropdown-item ' . $isactive . '" id="tab0">No results</a>';

        $tabs .= '<div class="tab-pane ' . $isactive . '" id="page0" role="tabpanel" aria-labelledby="tab0"><div class="card">
            <div class="card-body">
                <h3 class="card-title">No results to display.</h3>
            </div>
        </div>';
    }

if ($app_config['features']['add_new'] == 'enabled')
{

    
    $nav .= '
        <li class="nav-item">
            <a class="nav-link" id="tab-new" data-toggle="pill" data-target="#page-new" type="button" role="tab" aria-controls="page-new">Add New</a>
        </li>';

    $tabs .= '<div class="tab-pane" id="page-new" role="tabpanel" aria-labelledby="tab-new">
    
        <div class="container">
            <form id="add-form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="title" class="col-sm-4 col-form-label">Title</label>
                            <div class="col-sm-6">
                                <select class="custom-select mx-sm-3 w-50" id="title">
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Miss">Miss</option>
                                    <option value="Ms">Ms</option>
                                    <option value="CO">CO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="givenName1" class="col-sm-4 col-form-label">First Name</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" id="given-name" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="givenName2" class="col-sm-4 col-form-label">Middle Name</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" id="middle-name" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="familorganisationNameyName" class="col-sm-4 col-form-label">Organisation Name</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" name="orgName" id="org-name" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="familyName" class="col-sm-4 col-form-label">Last Name</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" name="familyName" id="family-name" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="initials" class="col-sm-4 col-form-label">Initials</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" id="initials" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="homePhone" class="col-sm-4 col-form-label">Home Phone</label>
                            <div class="col-sm-7">
                                <input type="tel" class="form-control mx-sm-3" id="home-phone" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="mobilePhone" class="col-sm-4 col-form-label">Mobile Phone</label>
                            <div class="col-sm-7">
                                <input type="tel" class="form-control mx-sm-3" name="mobile-phone" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="businessPhone" class="col-sm-4 col-form-label">Business Phone</label>
                            <div class="col-sm-7">
                                <input type="tel" class="form-control mx-sm-3" name="business-phone" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="emailAddress" class="col-sm-4 col-form-label">Email Address</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" id="email" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">

                        <strong>Physical Address</strong><br />
                        <div class="form-group row mt-2">
                            <label for="physicalAddress1" class="col-sm-4 col-form-label">Unit</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" id="physical-addr1" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="physicalAddress2" class="col-sm-4 col-form-label"> Street Address</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" name="physicalAddr2" id="physical-addr2" /><br />
                                <input type="text" class="form-control mx-sm-3" id="physical-addr3" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="physicalAddress5" class="col-sm-4 col-form-label">Town / City</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" name="physicalAddr4" id="physical-addr4" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="physicalAddress4" class="col-sm-4 col-form-label">Post Code</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-md-3 w-50" name="physicalAddr5" id="physical-addr5" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <strong>Postal Address</strong><br />
                        <div class="form-group row mt-2">
                            <label for="postAddress1" class="col-sm-4 col-form-label">Unit</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" id="post-addr1" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="postAddress2" class="col-sm-4 col-form-label"> Street Address</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" id="post-addr2" /><br />
                                <input type="text" class="form-control mx-sm-3" id="post-addr3" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="postAddress5" class="col-sm-4 col-form-label">Town / City</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" id="post-addr4" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="postAddress4" class="col-sm-4 col-form-label">Post Code</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3 w-50" id="post-addr5" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-sm-9 offset-sm-3">
                        <input type="submit" class="btn btn-success w-25" value="Add">
                        <input type="reset" class="btn btn-secondary w-25" value="Reset" id="reset-btn">
                    </div>
                </div>
                <div class="alert alert-success w-100" id="alert-add-success">
                    <strong>Success!</strong> User was added successfully.
                </div>
                <div class="alert alert-danger w-100" id="alert-add-failed">
                    <strong>Failed!</strong> There was a problem adding user to authority.
                </div>
	        </form>
        </div>
        </div>';

}

        
if ($app_config['features']['search_and_update'] == 'enabled')
{
    
    $nav .= '
        <li class="nav-item">
            <a class="nav-link" id="tab-search" data-toggle="pill" data-target="#page-search" type="button" role="tab" aria-controls="page-search">Search and Update</a>
        </li>';

    $tabs .= '

        <!-- search and update form -->

        <div class="tab-pane" id="page-search" role="tabpanel" aria-labelledby="tab-search">
            
            <div class="container">
                <form id="search-form">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Search</h2>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="givenName1" class="col-sm-4 col-form-label">First Name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control mx-sm-3" name="givenName" id="given-name-search" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="familyName" class="col-sm-4 col-form-label">Last Name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control mx-sm-3" name="familyName" id="family-name-search" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="familorganisationNameyName" class="col-sm-4 col-form-label">Organisation Name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control mx-sm-3" name="orgName" id="org-name-search" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mt-3">
                        <div class="col-sm-9 offset-sm-3">
                            <input type="submit" class="btn btn-success w-25" value="Search" id="search-btn">
                            <div class="ml-md-5" id="processing">
                                <span class="spinner-grow spinner-grow-sm text-primary"></span>
                                <span class="spinner-grow spinner-grow-sm text-primary"></span>
                                <span class="spinner-grow spinner-grow-sm text-primary"></span>
                                <span class="spinner-grow spinner-grow-sm text-primary"></span>
                                <span class="spinner-grow spinner-grow-sm text-primary"></span>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-success w-100" id="retrieve-record-success">
                            <strong>Success!</strong> Please select a record from the dropdown below.
                    </div>
                    <div class="alert alert-danger w-100" id="no-record-error">
                            <strong>Failed!</strong> No record was found.
                    </div>
                </form>

                <form id="update-form">
                    <div class="row">
                        <div class="col-md-12">
                            <hr />
                            <h2>Update</h2>
                        </div>
                    </div>

                    <div class="row" id="search-results">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="search_results" class="col-sm-4 col-form-label">Search Results</label>
                        <div class="col-sm-6">
                            <select class="custom-select mx-sm-3" id="search-items">
                                <option>--- Select a record ---</option>'
                                    . $updateopts .
                            '</select>
                        </div>
                    </div>
                </div>
            </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="homePhone" class="col-sm-4 col-form-label">Home Phone</label>
                            <div class="col-sm-7">
                                <input type="tel" class="form-control mx-sm-3" name="homePhone" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="mobilePhone" class="col-sm-4 col-form-label">Mobile Phone</label>
                            <div class="col-sm-7">
                                <input type="tel" class="form-control mx-sm-3" name="mobilePhone" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="businessPhone" class="col-sm-4 col-form-label">Business Phone</label>
                            <div class="col-sm-7">
                                <input type="tel" class="form-control mx-sm-3" name="businessPhone" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="emailAddress" class="col-sm-4 col-form-label">Email Address</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" name="email" value="" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <strong>Physical Address</strong><br />
                        <div class="form-group row mt-2">
                            <label for="physicalAddress1" class="col-sm-4 col-form-label">Unit</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" name="physicalAddr1" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="physicalAddress2" class="col-sm-4 col-form-label"> Street Address</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" name="physicalAddr2" value="" /><br />
                                <input type="text" class="form-control mx-sm-3" name="physicalAddr3" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="physicalAddress4" class="col-sm-4 col-form-label">Post Code</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control mx-sm-3 w-50" name="physicalAddr4" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="physicalAddress5" class="col-sm-4 col-form-label">Town / City</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" name="physicalAddr5" value="" />
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <strong>Postal Address</strong><br />
                        <div class="form-group row mt-2">
                            <label for="postAddress1" class="col-sm-4 col-form-label">Unit</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" name="postAddr1" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="postAddress2" class="col-sm-4 col-form-label"> Street Address</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" name="postAddr2" value="" /><br />
                                <input type="text" class="form-control mx-sm-3" name="postAddr3" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="postAddress4" class="col-sm-4 col-form-label">Post Code</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control mx-sm-3 w-50" name="postAddr4" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="postAddress5" class="col-sm-4 col-form-label">Town / City</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control mx-sm-3" name="postAddr5" value="" />
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="form-group row mt-3">
                        <div class="alert alert-success mx-auto w-75" id="alert-update-success">
                            <strong>Success!</strong> User was added successfully.
                        </div>
                        <div class="alert alert-danger mx-auto w-75" id="alert-update-failed">
                            <strong>Failed!</strong> There was a problem adding user to authority.
                        </div>
                        <div class="col-sm-9 offset-sm-3">
                            <input type="submit" class="btn btn-success w-25" value="Update" id="update-btn">
                            <input type="reset" class="btn btn-secondary w-25" value="Reset" id="reset-btn">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        ';

}

        $nav .= '
        </ul>
        </div>
        </nav></div>';

        $searchStr = '';
        if (isset($_GET['phone'])) {
            $searchStr .= '<input type="hidden" name="phone" value="' . $_GET['phone'] . '" />';
        } else if (isset($_GET['email'])) {
            $searchStr .= '<input type="hidden" name="email" value="' . $_GET['email'] . '" />';
        } else {
            if (isset($_GET['firstname'])) {
                $searchStr .= '<input type="hidden" name="firstname" value="' . $_GET['firstname'] . '" />';
            }

            if (isset($_GET['lastname'])) {
                $searchStr .= '<input type="hidden" name="lastname" value="' . $_GET['lastname'] . '" />';
            }
        }

        $html .= '<header>' . $nav . '</header><br><main><div class="container"><form>' . $searchStr . 'Show last <select class="custom-select w-25" name="showYears" style="width: 6%!important;">
            <option value=1 ';
        if ($number_of_years_history == 1) {
            $html .= 'selected';
        }
            $html .= '>1</option>
                <option value=2 ';
        if ($number_of_years_history == 2) {
            $html .= 'selected';
        }
            $html .= '>2</option>
                <option value=3 ';
        if ($number_of_years_history == 3) {
            $html .= 'selected';
        }
            $html .= '>3</option>
                <option value=4 ';
        if ($number_of_years_history == 4) {
            $html .= 'selected';
        }
            $html .= '>4</option>
                <option value=5 ';
        if ($number_of_years_history == 5) {
            $html .= 'selected';
        }
            $html .= '>5</option>
                <option value=6 ';
        if ($number_of_years_history == 6) {
            $html .= 'selected';
        }
            $html .= '>6</option>
                <option value=7 ';
        if ($number_of_years_history == 7) {
            $html .= 'selected';
        }
            $html .= '>7</option>
                <option value=8 ';
        if ($number_of_years_history == 8) {
            $html .= 'selected';
        }
            $html .= '>8</option>
                <option value=9 ';
        if ($number_of_years_history == 9) {
            $html .= 'selected';
        }
            $html .= '>9</option>
                <option value=10 ';
        if ($number_of_years_history == 10) {
            $html .= 'selected';
        }
            $html .= '>10</option>
                <option value=11 ';
        if ($number_of_years_history == 11) {
            $html .= 'selected';
        }
            $html .= '>11</option>
                <option value=12 ';
        if ($number_of_years_history == 12) {
            $html .= 'selected';
        }
            $html .= '>12</option>
            </select> years <input class="btn btn-primary" type="submit" /></form>' . $tabs . '</main>';
    }

    $html .= '
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="text/javascript" src="loadRecords.js"></script>
    <script type="text/javascript" src="custom.js"></script>
    </body>
    <style>
    .listItem, .details {
        display: none;
    }

    .listItem button {
        margin-left: 20px;
    }
    </style></html>';

    echo $html;
