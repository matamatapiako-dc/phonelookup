let narList = [];

$(document).ready(function () {
	$("#processing").hide();
	$("#alert-add-success").hide();
	$("#alert-add-failed").hide();
	$("#alert-update-success").hide();
	$("#alert-update-failed").hide();
	$("#retrieve-record-success").hide();
	$("#no-record-error").hide();
	$("#family-name").attr("required", "required");
	$("#org-name").attr("disabled", "disabled");
	//$("#search-results").hide();

	$(".dropdown-menu a").click(function () {
		let id = $(this).attr("id").substr(3);
		let tabId = "tab" + id;
		let pageId = "page" + id;
		$(".active").removeClass("active");
		$("#" + tabId + "").addClass("active");
		$("#" + pageId + "").addClass("active");
		$(".dropdown-toggle").addClass("active");
	});

	$("#title").change(function () {
		if (this.value == "CO") {
			$("#org-name").removeAttr("disabled");
			$("#family-name").attr("disabled", "disabled");
			$("#given-name").attr("disabled", "disabled");
			$("#middle-name").attr("disabled", "disabled");
			$("#initials").attr("disabled", "disabled");
		} else {
			$("#family-name").removeAttr("disabled");
			$("#given-name").removeAttr("disabled");
			$("#middle-name").removeAttr("disabled");
			$("#initials").removeAttr("disabled");
			$("#org-name").attr("disabled", "disabled");
		}
	});

	// Add form validation
	$("#add-form").validate({
		rules: {
			familyName: {
				required: true
			},
			orgName: {
				required: true
			},
			physicalAddr2: {
				required: true
			},
			physicalAddr4: {
				required: true
			},
			physicalAddr5: {
				required: true
			}
		},
		messages: {
			familyName: {
				required: '<span class="text-danger pl-sm-4">Family name is mandatory</span>'
			},
			orgName: {
				required: '<span class="text-danger pl-sm-4">Organisation Name is mandatory</span>'
			},
			physicalAddr2: {
				required: '<span class="text-danger pl-sm-4">Street Address is mandatory</span>'
			},
			physicalAddr4: {
				required: '<span class="text-danger pl-sm-4">Town / City is mandatory</span>'
			},
			physicalAddr5: {
				required: '<span class="text-danger pl-sm-4">Post Code is mandatory</span>'
			}
		},
		submitHandler: function (form) {
			addNewRecord();
		}
	});

	// Search form validation
	$("#search-form").validate({
		rules: {
			familyName: {
				required: true
			},
			orgName: {
				required: true
			}
		},
		messages: {
			familyName: {
				required: '<span class="text-danger pl-sm-4">Family name is mandatory</span>'
			},
			orgName: {
				required: '<span class="text-danger pl-sm-4">Organisation Name is mandatory</span>'
			}
		},
		submitHandler: function (form) {
			searchRecord();
		}
	});

	$("#search-btn").click(function () {
		$("#processing").show();
		$("#retrieve-record-success").hide();
		$("#no-record-error").hide();
		$("#search-results").hide();

		let orgNameVal = $('#org-name-search').val().trim();
		let familyNameVal = $('#family-name-search').val().trim();
		let givenNameVal = $('#given-name-search').val().trim();

		if (orgNameVal != "" || familyNameVal != "" || givenNameVal != "") {
			$("#family-name-search").rules("remove");
			$("#org-name-search").rules("remove");
		} else {
			$("#org-name-search").rules("add", {
				required: true
			});
			$("#family-name-search").rules("add", {
				required: true
			});
		}
	});

	$("#search-items").change(function () {
		let selectedIndex = this.value;

		let selectedNar = narList.find(function (item, index) {
			return index == selectedIndex;
		});

		console.log(selectedNar);
	});

	$("#reset-btn").click(function () {
		$("form").trigger("reset");
		$("#family-name").removeAttr("disabled");
		$("#given-name").removeAttr("disabled");
		$("#middle-name").removeAttr("disabled");
		$("#initials").removeAttr("disabled");
		$("#org-name").attr("disabled", "disabled");
	});
});

// Adding new record
function addNewRecord() {
	$.post("add_new_record.php",
		{
			familyName: $("#family-name").val().trim(),
			givenName: $("#given-name").val().trim(),
			middleName: $("#middle-name").val().trim(),
			initials: $("#initials").val().trim(),
			title: $("#title").val().trim(),
			orgName: $("#org-name").val().trim(),
			homePhone: $("#home-phone").val().trim(),
			mobilePhone: $("#mobile-phone").val().trim(),
			businessPhone: $("#business-phone").val().trim(),
			email: $("#email").val().trim(),
			physicalAddr1: $("#physical-addr1").val().trim(),
			physicalAddr2: $("#physical-addr2").val().trim(),
			physicalAddr3: $("#physical-addr3").val().trim(),
			physicalAddr4: $("#physical-addr4").val().trim(),
			physicalAddr5: $("#physical-addr5").val().trim(),
			postAddr1: $("post-addr1").val().trim(),
			postAddr2: $("post-addr2").val().trim(),
			postAddr3: $("post-addr3").val().trim(),
			postAddr4: $("post-addr4").val().trim(),
			postAddr5: $("post-addr5").val().trim()
		},
		function (data, status) {

			if (data == "true") {
				$("#alert-add-success").show();
			} else {
				$("#alert-add-failed").show();
			}

		});
}

// Searching for record
function searchRecord() {
	$.post("search_records.php",
		{
			orgName: $("#org-name-search").val().trim(),
			familyName: $("#family-name-search").val().trim(),
			givenName: $("#given-name-search").val().trim()
		},
		function (data, status) {
			$("#processing").hide();
			if (data != "false") {
				$("#search-results").show();
				narList = jQuery.parseJSON(data);

				// Removing items with null givenName1 and familyName
				narList.forEach(function (item, index) {
					if (item.givenName1 == null && item.familyName == null) {
						narList.splice(index, 1);
					}
				});
				if (narList.length == 0) {
					$("#retrieve-record-success").hide();
					$("#search-results").hide();
					$("#no-record-error").show();
				} else {
					$(".result-item").remove();

					// Populate results in the dropdown list
					narList.forEach(function (item, index) {
						$("#search-items").append('<option class="result-item" value=' + index + '>' + item.title + ' ' + item.givenName1 + ' ' + item.familyName + '</option>');
					});
					$("#retrieve-record-success").show();
				}
			} else {
				$("#retrieve-record-success").hide();
				$("#search-results").hide();
				$("#no-record-error").show();
			}
		});
}