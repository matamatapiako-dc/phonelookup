# PhoneLookup



## Phone Lookup for Mitel Integration

URL: http://intapps.mpdc.govt.nz/phonelookup/

## Changes 29 April 2022

- Handling Family Name with special character when searching for NAR records
- Implemented searching for NAR record to search for item that starts with the search string rather than the exact string

## Changes 4 October 2022

- Added filtering to display CRM based on the last number of years