<?php
require_once("../authorityphp/authority.php");

$authority = new authority();
$nar_api = $authority->login();
$name = $nar_api->initialise('NA');

$physical_addr1 = $_REQUEST['physicalAddr1'] . " " . $_REQUEST['physicalAddr2'] . " " . $_REQUEST['physicalAddr3'];
$physical_addr2 = $_REQUEST['physicalAddr4'] . " " . $_REQUEST['physicalAddr5'];

$postal_addr1 = $_REQUEST['postalAddr1'] . " " . $_REQUEST['postalAddr2'] . " " . $_REQUEST['postalAddr3'];
$postal_addr2 = $_REQUEST['postalAddr4'] . " " . $_REQUEST['postalAddr5'];

$result = $name->postNarRecord(
	$_REQUEST['title'],
	$_REQUEST['familyName'],
	$_REQUEST['givenName'],
	$_REQUEST['middleName'],
	$_REQUEST['initials'],
	$_REQUEST['orgName'],
	$physical_addr1,
	$physical_addr2,
	$postal_addr1,
	$postal_addr2,
	$_REQUEST['homePhone'],
	$_REQUEST['mobilePhone'],
	$_REQUEST['businessPhone'],
	$_REQUEST['email']
);

if ($result) {
	$fh = fopen("logs/nars_added_" . date("Ymd") . ".txt", "a");
	$log_string = date("H:i:s") . "\tNAR Added for " . $_REQUEST['title'] . " " . $_REQUEST['givenName'] . " " . $_REQUEST['familyName'] . " " . $_REQUEST['orgName']
		. " as NAR Number " . $result['message'] . "\n";
	fwrite($fh, $log_string);
	fclose($fh);

	echo "true";
}
