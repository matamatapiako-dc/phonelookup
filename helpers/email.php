<?php
require_once("../authorityphp/authority.php");

class EmailHelper
{
    private $value;
    private $narRecords;
    private $authority;

    function __construct($emailAddress)
    {
        $this->value = $emailAddress;

        $this->authority = new authority();
        $this->authority->login();

        $this->setNarRecords($this->value);
    }

    public function getEmailValue()
    {
        return $this->value;
    }

    public function getNarRecords()
    {
        return $this->narRecords;
    }

    private function setNarRecords($value)
    {
        $filter = "emailAddress eq '$value'";
        $nar = $this->authority->initialise('NA');
        $this->narRecords = $nar->getNarRecords($filter, 'givenName1, familyName');
    }
}
