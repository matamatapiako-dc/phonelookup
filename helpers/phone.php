<?php
require_once("../authorityphp/authority.php");

class PhoneHelper
{
    private $value;
    private $narRecords;
    private $authority;

    function __construct($phoneNumber)
    {
        $area_code = substr($phoneNumber, 0, 2);

        switch ($area_code) {
            case 02:
                //is a mobile number
                $phoneNumber = str_replace(" ", "", $phoneNumber);
                $this->value = substr($phoneNumber, 0, 3) . " " .  substr($phoneNumber, 3, strlen($phoneNumber) - 3);
                break;
            case 05:
                //could be a 0508 number
                if (substr($phoneNumber, 0, 4) == '0508') {
                    //0508 436 638
                    $phoneNumber = str_replace(" ", "", $phoneNumber);
                    $this->value = substr($phoneNumber, 0, 4) . "" .  substr($phoneNumber, 4, strlen($phoneNumber) - 4);
                }
                break;
            case 07:
                //is a landline
            default:
                // or we don't know
                $phoneNumber = str_replace(" ", "", $phoneNumber);
                $this->value = substr($phoneNumber, 0, 2) . " " .  substr($phoneNumber, 2, 3) . " " . substr($phoneNumber, 5, strlen($phoneNumber) - 5);
                break;
        }

        $this->authority = new authority();
        $this->authority->login();

        $this->setNarRecords($this->value);
    }

    public function getPhoneValue()
    {
        return $this->value;
    }

    public function getNarRecords()
    {
        return $this->narRecords;
    }

    private function setNarRecords($value)
    {
        $filter = "homePhone1 eq '$value' OR homePhone2 eq '$value' OR mobilePhone eq '$value' OR businessPhone eq '$value'";
        $nar = $this->authority->initialise('NA');
        $this->narRecords = $nar->getNarRecords($filter, 'givenName1, familyName');
    }
}
