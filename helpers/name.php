<?php
require_once("../authorityphp/authority.php");

class NameHelper
{
    private $givenName;
    private $familyName;
    private $narRecords;
    private $authority;

    function __construct($firstName, $lastName)
    {
        $this->givenName = $firstName;
        $this->familyName = $lastName;

        $this->authority = new authority();
        $this->authority->login();

        $this->setNarRecords($this->givenName, $this->familyName);
    }

    public function getFirstNameValue()
    {
        return $this->givenName;
    }

    public function getLastNameValue()
    {
        return $this->familyName;
    }

    public function getNarRecords()
    {
        return $this->narRecords;
    }

    private function setNarRecords($givenName, $familyName)
    {
        $givenName = urlencode($givenName);
        $familyName = urlencode($familyName);

        if ($givenName && $familyName) {
            $filter = "familyName eq '$familyName' AND givenName1 eq '$givenName'";
        } else if ($givenName) {
            $filter = "givenName1 eq '$givenName'";
        } else if ($familyName) {
            $filter = "familyName eq '$familyName'";
        } else {
            die();
        }

        $nar = $this->authority->initialise('NA');
        $this->narRecords = $nar->getNarRecords($filter, 'givenName1, familyName');
    }
}
