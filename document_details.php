<?php


include_once("../authorityphp/authority.php");

$authority = new authority();
$authority->login();

$docs = new authority_documents($authority);
$prop = new authority_propertyaddress($authority);
$crmCatergories = new authority_categories($authority);
$crmDescription = new authority_crm($authority);
$workflowTask = new authority_tasks($authority);
$workflowAction = new authority_workflowactions($authority);
$response = array("html" => "");

if (isset($_GET['account'])) {
  $results = $docs->getDocuments("formattedAccount eq '" . $_GET['account'] . "'");
  $doc = array_pop($results);
  $tasks = $workflowTask->tasks("formattedAccount eq '" . $_GET['account'] . "'");
  $attributes = $docs->getDocumentAttributes($doc['id']);
  $propLinks = $docs->getDocumentPropertyLinks($doc['id']);
  $description = $crmDescription->crmDescription($doc['id'])['description'];
 // $response['desc'] = print_r($description, true);

  $response['doc'] = print_r($doc, true);
  /*$response['tasks'] = print_r($tasks, true);
  $response['attributes'] = print_r($attributes, true);
  $response['propLinks'] = print_r($propLinks, true);*/

  $response['html'] .= '<p id="details_' . $doc['formattedAccount'] . '" class="jumbotron details hidden">
                              <strong>' . $doc['documentPrecis'] . '</strong><br />' . $description . '<br/>
                              <strong>Date Received:</strong> ' . date('d/m/Y H:i', strtotime($doc['createdDatetime'])) . '<br/> 
                              <strong>Date Updated:</strong> ' . date('d/m/Y H:i', strtotime($doc['modifiedDatetime'])) . '<br/>';                          
  $response['html'] .= '<strong>Status:</strong> ' . $doc['determinationCode'] . '<br />';

  foreach ($propLinks as $pl) {
    $propAddr = $prop->getPropertyAddress('parcelId eq ' . $pl['parcelId'])[0];

    $address_str = ($propAddr['unitNumber'] != '' ? 'Unit ' . $propAddr['unitNumber'] : '');
    $address_str .= ($propAddr['unitAlpha'] != '' ? $propAddr['unitAlpha'] . ' ' : ' ');
    $address_str .= ($propAddr['houseNumber'] != '' ? $propAddr['houseNumber'] : '');
    $address_str .= ($propAddr['houseAlpha'] != '' ? $propAddr['houseAlpha'] : '');
    $address_str .= ($propAddr['houseEndNumber'] != '' ? '-' . $propAddr['houseEndNumber'] : '');
    $address_str .= ($propAddr['houseEndAlpha'] != '' ? $propAddr['houseEndAlpha'] . ' ' : ' ');
    $address_str .= ($propAddr['streetName'] != '' ? $propAddr['streetName'] . ' ' : ' ');
    $address_str .= ($propAddr['streetTypeDescription'] != '' ? $propAddr['streetTypeDescription'] . ', ' : ', ');
    $address_str .= ($propAddr['suburb'] != '' ? $propAddr['suburb'] . ' ' : ' ');
    $address_str .= ($propAddr['postcode'] != '' ? $propAddr['postcode'] . ' ' : ' ');

    $response['html'] .= '<strong>Parcel No:</strong> ' . $pl['parcelId'] . ' (' . $address_str . ')<br />';
  }

  if ($doc['categoryId']) {
    $categoryDef = $crmCatergories->getCrmCategories('id eq ' . $doc['categoryId'])[0]['categoryDescription'];

    $response['html'] .= '<strong>CRM Category:</strong> ' . $categoryDef . '<br />';
  }

  foreach ($tasks as $t) {
    $workflowActionDef = $workflowAction->getWorkflowActionByActionCode($t['workflowActionCode']);

    $response['html'] .= '<strong>Current Task:</strong> ' . $workflowActionDef['fullDescription'] . ' - Status ' . $t['determinationCode'] . '<br />';
  }

  //$recDate = $results

  /*
  foreach ($attributes as $attrib) {
    if (isset($attrib['label']) && $attrib['label'] != "") {
      $response['html'] .= '<strong>' . $attrib['label'] . '</strong> ' . $attrib['value'] . '<br />';
    }
  }
  */

  $response['html'] .= '</p>';

     $recDate = date('d/m/Y', strtotime($doc['receivedDatetime']));

  $response['label'] = $recDate . ' - ' . (!$categoryDef ? $doc['documentPrecis'] : $categoryDef) . ' - ' . $doc['contactMethodCode'];
}

echo json_encode($response);
