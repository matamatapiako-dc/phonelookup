$(document).ready(function () {
    var id = $(".dropdown-item.active").attr("id").replace("tab", "");
    loadItems(id);

    $(".dropdown-item").not(".active").click(function(){
        var id = $(this).attr("id").replace("tab", "");
        console.log("changed page to " + id);
        loadItems(id);
    });
});

function loadItems(id) {
    var count = 1;
    $("#page"+id+" .listItem").each(function () {
        var fma = $(this).attr("data-fma");
        var id = $(this).attr("id");
        var listItem = $(this);

        listItem.html('<img style="width: 20px;" src="loading.gif" alt="Loading record info">').show();

        $.ajax({
            url: "document_details.php?account=" + fma,
            dataType: "json",
            success: function (response) {
                listItem.html(response.label + '<button class="btn btn-primary detailBtn" onclick="toggleDetails(\'' + id + '\')">Show details</button>' + response.html);
                count++;

                if (count == $('.listItem').length) {
                    var oldtext = $('#recordCount').text();
                    $('#recordCount').html('<strong>' + oldtext.replace('Loading record data...', '') + '</strong>');
                }
            }
        });
    });
}

function toggleDetails(id) {
   
    console.log($('#' + id + ' .details'));
    $('#' + id + ' .details').slideToggle("1500", function () {
        if ($('#' + id + ' .detailBtn').is('.open')) {
            $('#' + id + ' .detailBtn').removeClass('open').addClass('closed').text('Show details');
        } else {
            $('#' + id + ' .detailBtn').removeClass('closed').addClass('open').text('Hide details');
        }
    });
}
